FROM mcr.microsoft.com/dotnet/core/sdk:3.1
LABEL maintainer="corentin.altepe@gmail.com"

RUN apt-get update && apt-get install -y openjdk-11-jre-headless && apt-get clean