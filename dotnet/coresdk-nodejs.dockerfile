FROM mcr.microsoft.com/dotnet/core/sdk:3.1
LABEL maintainer="corentin.altepe@gmail.com"

# Install NodeJs v12.13.1 on top of the dotnet core sdk
RUN curl -SL "https://nodejs.org/dist/v12.13.1/node-v12.13.1-linux-x64.tar.gz" \
    --output nodejs.tar.gz && echo "074a6129da34b768b791f39e8b74c6e4ab3349d1296f1a303ef3547a7f9cf9be nodejs.tar.gz" \
    | sha256sum -c - && tar -xzf "nodejs.tar.gz" -C /usr/local \
    --strip-components=1 && \
    rm nodejs.tar.gz && ln -s /usr/local/bin/node /usr/local/bin/nodejs